//
//  ViewController.swift
//  AngryHerds
//
//  Created by Mike Laursen on 6/15/20.
//  Copyright © 2020 Appamajigger. All rights reserved.
//

import SpriteKit
import UIKit

class ViewController: UIViewController {
    var bullNode: BullNode? = nil
    var castlePosition = CGPoint()
    var scene: SKScene? = nil
    
    class BullNode: SKSpriteNode {
        override var isUserInteractionEnabled: Bool {
            get {
                return true
            }
            set {

            }
        }
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.run(SKAction.rotate(byAngle: 2 * .pi, duration: 0.3))
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.scene = SKScene(size: view.bounds.size)
        guard let scene = self.scene else {
            preconditionFailure()
        }
        scene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        scene.backgroundColor = .systemBackground
        
        bullNode = BullNode(imageNamed: "angrybull")
        guard let bullNode = bullNode else {
            preconditionFailure()
        }
        bullNode.zPosition = 1.0
        scene.addChild(bullNode)
        let castleNode = SKSpriteNode(imageNamed: "castle")
        scene.addChild(castleNode)
        let skView = self.view as! SKView
        skView.presentScene(scene)

        let bullPositionInUIKitSpace = CGPoint(x: self.view.bounds.width / 2.0, y: 2.0 * self.view.bounds.height / 3.0)
        // convertPoint() only gives correct results after presentScene().
        let bullPositionInSKSpace = scene.convertPoint(fromView: bullPositionInUIKitSpace)
        bullNode.position = bullPositionInSKSpace

        castlePosition = scene.convertPoint(fromView: CGPoint(x: self.view.bounds.width / 2.0, y: self.view.bounds.height / 4.0))
        castleNode.position = castlePosition
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let scene = self.scene, let bullNode = self.bullNode else {
            preconditionFailure()
        }
        if let touch = touches.first {
            if bullNode.contains(touch.location(in: scene)) {
                bullNode.run(SKAction.move(to: castlePosition, duration: 0.3))
//                bullNode.run(SKAction.move(to: castlePosition, duration: 0.3)) {
//                    if let emitterNode = SKEmitterNode(fileNamed: "Boom") {
//                        emitterNode.position = self.castlePosition
//                        scene.addChild(emitterNode)
//                        bullNode.removeFromParent()
//                    }
//                }
            }
        }
    }


}

