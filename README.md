# README #

The "AngryHerds" iOS app was created as a demo of how to use the Apple SpriteKit framework to be given to the SwiftBoise meetup group.

### Presentation ###

* YouTube video of SwiftBoise presentation: https://youtu.be/cI8A0I-BqIc

### How do I get set up? ###

* The app was created with Xcode 11.3.
* Just open the project. If significant time has passed since the app was created, Xcode may ask to update the code.

### SpriteKit Info ###

* Apple's SpriteKit documentation: https://developer.apple.com/documentation/spritekit

### Contribution guidelines ###

* This app is dedicated to the public domain: https://choosealicense.com/licenses/unlicense/

### Who do I talk to? ###

* mike@appamajigger.com