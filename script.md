\[ \] Introduce concept of sprites:

- [What is a sprite?](https://image.slidesharecdn.com/spritekit-160422155713/95/introduction-to-sprite-kit-5-638.jpg?cb=1461340868)
- Come from arcade games. Atari. Special video circuitry. Scratch.

\[ \] Open Xcode project:

- Check out L1.
- Show SpriteKit framework: General > Targets > AngryHerds > General > Frameworks
- Show SceneDelegate.swift.
    
\[ \] SKNodes, SKScenes and SKSpriteNodes:

- Check out L2.
- [Tree of SKNodes](https://image.slidesharecdn.com/spritekit-160422155713/95/introduction-to-sprite-kit-11-638.jpg?cb=1461340868)
- Show SKView in storyboard.
- Import SpriteKit.
- Add "scene" and "bullNode" in ViewController.swift. Run.
- Set scene.anchorPoint. Run.
- Set background color. Run.

\[ \] Hit testing:

- Check out L3.
- Explain hit testing.
- Have to enable user interaction.
- Optional: Note we are using radians, not degrees, in angles.
- Optional: Point out subclass init() quirk.
- Can also do drag of sprites, but not covering it today.

\[ \] Castle + Coordinate conversion:

- Check out L4.
- [UIKit coordinates](https://developer.apple.com/library/archive/documentation/2DDrawing/Conceptual/DrawingPrintingiOS/GraphicsDrawingOverview/GraphicsDrawingOverview.html)
- [SpriteKit coordinates](https://developer.apple.com/documentation/spritekit/sknode/about_spritekit_coordinate_systems)]
- Add castleNode.
- Set castlePosition.
- Optional: Show conversion in debugger.
- Optional: Show on device with different screen size.
- SKScene can also do scaling, but not covering it today.

\[ \] Add movement:

- Check out L5.
- Make castlePostion a class variable.
- Add ViewController.touchesBegan(). (Leave zPosition commented out.)
- Explain zPosition.
- Explain multiple touchesBegan(): [Tree of SKNodes](https://image.slidesharecdn.com/spritekit-160422155713/95/introduction-to-sprite-kit-11-638.jpg?cb=1461340868)

\[ \] Add Effects!:

- Check out L6.
- Add SKEmmitterNode node.
- Set position of emitter node.
- Optional: Create new SpriteKit Particle File.

\[ \] Presentation doesn't cover:
* SKLabelNode, SKParticleNode, SKVideoNode, SKCropNode
* Physics simulation and collisions
* SKTexture

\[ \] References:

- <https://bitbucket.org/mvlaursen/angryherds>
- <https://developer.apple.com/documentation/spritekit>